from WebAppDIRAC.Lib.WebHandler import WebHandler, WErr, WOK, asyncGen
from DIRAC import gConfig, S_OK, S_ERROR, gLogger
from DIRAC.Core.DISET.RPCClient import RPCClient
import json

class SpaceOccupancyHandler( WebHandler ):

  AUTH_PROPS = 'authenticated'

  @asyncGen
  def web_getSelectionData( self ):
    callback = {
                 'token'    : set(),
                 'site'     : set()
                }
     
    pub = RPCClient( 'ResourceStatus/Publisher' )
        
    gLogger.info( self.request.arguments )    
    
    spaces = yield self.threadTask(pub.getSpaceTokenOccupancy, None, None )
    
    if spaces[ 'OK' ]:
        
      for sp in spaces[ 'Value' ]:
      
        callback[ 'token' ].add( sp[ 'Token' ] )
        callback[ 'site' ].add( sp[ 'Site' ] )
                
      
    for key, value in callback.items():
    
      callback[ key ] = [ [ item ] for item in list( value ) ]
      callback[ key ].sort()
      callback[ key ] = [ [ 'All' ] ] + callback[ key ] 
          
    self.finish(callback)

  
  @asyncGen
  def web_getSpaceOccupancyData ( self ):
    
    requestParams = self.__requestParams()
    gLogger.info( requestParams )

    pub = RPCClient( 'ResourceStatus/Publisher' )
    
    res = yield self.threadTask(pub.getSpaceTokenOccupancy, requestParams[ 'site' ], requestParams[ 'token' ] )
    
    if not res[ 'OK' ]:
      raise WErr.fromSERROR( res )
       
    for sp in res[ 'Value' ]:

      sp[ 'LastCheckTime' ] = str( sp[ 'LastCheckTime' ] )
      
      if sp[ 'Total' ]:
        sp[ 'Ratio' ] = float( '%.2f' % ( sp[ 'Free' ] * 100 / sp[ 'Total' ] ) )
      else:
        sp[ 'Ratio' ] = '-'    
      
      sp[ 'Free' ]          = float( '%.2f' % sp[ 'Free' ] )
      sp[ 'Total' ]         = float( '%.2f' % sp[ 'Total' ] )
      sp[ 'Guaranteed' ]    = float( '%.2f' % sp[ 'Guaranteed' ] )
    
    
      #FIXME: call here SpaceTokenOccupancyPolicy and avoid hardcoding twice
    
      if sp[ 'Total' ] == 0:
        sp[ 'Status' ] = 'Unknown'
      elif sp[ 'Free' ] < 0.1:
        sp[ 'Status' ] = 'Banned'
      elif sp[ 'Free' ] < 5:
        sp[ 'Status' ] = 'Degraded'
      else:
        sp[ 'Status' ] = 'Active'      
      
    self.finish( { 'success': "true", 'result': res[ 'Value' ], 'total': len( res[ 'Value' ] ) } )
      
  def __requestParams( self ):
    '''
      We receive the request and we parse it, in this case, we are doing nothing,
      but it can be certainly more complex.
    '''
    
    gLogger.always( "!!!  PARAMS: ", str( self.request.arguments ) ) 
    
    responseParams = { 
                      'token'    : None,
                      'site'     : None
                     }
    
    for key in responseParams.keys():
      if key in self.request.arguments and str( self.request.arguments[ key ][-1] ):
        responseParams[ key ] = list( json.loads( self.request.arguments[ key ][-1] ) )   
    
    return responseParams    
  
