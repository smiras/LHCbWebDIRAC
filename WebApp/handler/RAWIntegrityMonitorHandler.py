from WebAppDIRAC.Lib.WebHandler import WebHandler, WErr, WOK, asyncGen
from DIRAC.Core.DISET.RPCClient import RPCClient
from WebAppDIRAC.Lib.SessionData import SessionData
from DIRAC import gConfig, S_OK, S_ERROR, gLogger
from WebAppDIRAC.WebApp.handler.Palette import Palette

import datetime, tempfile, json
from datetime                                        import timedelta
from time                                            import time, gmtime, strftime

from DIRAC.AccountingSystem.Client.ReportsClient     import ReportsClient
from DIRAC.Core.Utilities.DictCache                  import DictCache
from DIRAC.Core.Utilities import Time


class RAWIntegrityMonitorHandler( WebHandler ):
  
  AUTH_PROPS = "authenticated"

  numberOfFiles = 25
  pageNumber = 0
  globalSort = [["SubmitTime", "DESC"]]
  
  def index( self ):
    pass

  
  @asyncGen
  def web_getSelectionData( self ):
    
    callback = {}
    now = datetime.datetime.today()
    mon = timedelta( days = 30 )
    sd = now - mon
    tmp = {}
    ttt = str( sd.isoformat() )
    gLogger.info( " - T I M E - ", ttt )
    tmp["startDate"] = sd.isoformat()
    tmp["startTime"] = sd.isoformat()
    callback["extra"] = tmp
    if len( self.request.arguments ) > 0:
      tmp = {}
      for i in self.request.arguments :
        tmp[i] = str( self.request.arguments[i] )
      callback["extra"] = tmp
    #####################################################################
    # This is the part that optains the selections from the integrity db.
    RPC = RPCClient( "DataManagement/RAWIntegrity" )
   
    result = yield self.threadTask( RPC.getFileSelections )
  
    if result["OK"]:
      if len( result["Value"] ) > 0:
        result = result["Value"]
        for key, value in result.items():
          if len( value ) > 3:
            value = ["All"] + value
          key = key.lower()
          value = map( lambda x: [x], value )
          callback[key] = value
    else:
      callback = {"success":"false", "error": result["Message"]}  
    gLogger.info( " - callback - ", callback )
    self.finish( callback )

  def __request( self ):
    
    
    req = {}
    
    if "lfn" in self.request.arguments and len( self.request.arguments["lfn"] ) > 0: 
      self.pageNumber = 0
      lfns = list( json.loads( self.request.arguments[ 'lfn' ][-1] ) ) 
      if ( len( lfns ) > 0 ):
        req["lfn"] = lfns
        
    if "limit" in self.request.arguments:
      self.numberOfFiles = int( self.request.arguments["limit"][-1] )
      if "start" in self.request.arguments:
        self.pageNumber = int( self.request.arguments["start"][-1] )
      else:
        self.pageNumber = 0
    else:
      self.numberOfFiles = 25
      self.pageNumber = 0
    
      
    #######################################################################
    # For the selection boxes only
    if "status" in self.request.arguments:
      req["Status"] = list( json.loads( self.request.arguments[ 'status' ][-1] ) )
      
    if "storageelement" in  self.request.arguments:
      if str( request.params["storageelement"] ) != "All":
        req["StorageElement"] = list( json.loads( self.request.arguments[ 'storageelement' ][-1] ) ) 
    #######################################################################
    # For the start time selection
    if 'startDate' in self.request.arguments and len( self.request.arguments["startDate"][0] ) > 0:
      if 'startTime' in self.request.arguments and len( self.request.arguments["startTime"][0] ) > 0:
        req["FromDate"] = str( self.request.arguments["startDate"][0] + " " + self.request.arguments["startTime"][0] )
      else:
        req["FromDate"] = str( self.request.arguments["startDate"][0] )

    if 'endDate' in self.request.arguments and len( self.request.arguments["endDate"][0] ) > 0:
      if 'endTime' in self.request.arguments and len( self.request.arguments["endTime"][0] ) > 0:
        req["ToDate"] = str( self.request.arguments["endDate"][0] + " " + self.request.arguments["endTime"][0] )
      else:
        req["ToDate"] = str( self.request.arguments["endDate"][0] )

    
    #######################################################################
    # The global sort of the data
    if 'sort' in self.request.arguments:
      sort = json.loads( self.request.arguments['sort'][-1] )
      if len( sort ) > 0:
        self.globalSort = []
        for i in sort :
          self.globalSort += [[i['property'], i['direction']]]
    else:
      self.globalSort = [["SubmitTime", "DESC"]]
      
    gLogger.info( "REQUEST:", req )
    return req

  @asyncGen
  def web_getloggingInfo( self ):
    req = self.__request()
    gLogger.debug( "getloggingInfo" + str( req ) )
    
    RPC = RPCClient( "DataManagement/DataLogging" ) 
    result = yield self.threadTask( RPC.getFileLoggingInfo, str( req['lfn'][0] ) )
    
    if not result["OK"]:
      self.finish( {"success":"false", "error":result["Message"]} )
    result = result["Value"]
    if not result:
      self.finish( {"success":"false", "result":"", "error":"No logging information found for LFN"} )
    callback = []
    for i in result:
      callback.append( [i[0], i[1], i[2], i[3]] )
    self.finish( {"success":"true", "result":callback} )
    

  @asyncGen
  def web_getStatisticsData( self ):
    paletteColor = Palette()
    gLogger.debug( "Params:" + str( self.request.arguments ) )
    
    req = self.__request()
    selector = self.request.arguments["statsField"][0]

    if selector == 'Status':
      selector = "status"
    if selector == "Storage Element":
      selector = "storageelement"
      
    RPC = RPCClient( "DataManagement/RAWIntegrity" )
    
    result = yield self.threadTask( RPC.getStatistics, selector, req )
    gLogger.info( " - result - :", result )
    callback = {}
    
    if result["OK"]:
      callback = []
      result = result["Value"] 
      keylist = result.keys()
      keylist.sort()
      
      for key in keylist:
        callback.append( {"key":key, "value":result[key], "code":"", "color": paletteColor.getColor( key ) } )
      callback = {"success":"true", "result":callback}
    else:
      callback = {"success":"false", "error":result["Message"]}
      
    gLogger.debug( "retValue" + str( callback ) )
    self.finish( callback )
      

  #####################################################################
  #  
  # Handles displaying results
  #  
  @asyncGen
  def web_getRawIntegrityData( self ):
    gLogger.info( " -- SUBMIT --" )
    pagestart = time()
    RPC = RPCClient( "DataManagement/RAWIntegrity" )
    result = self.__request()
    result = yield self.threadTask( RPC.getFilesSummaryWeb, result, self.globalSort, self.pageNumber, self.numberOfFiles )
    if result["OK"]:
      result = result["Value"]
      
      if result.has_key( "TotalRecords" ):
        if  result["TotalRecords"] > 0:
          if result.has_key( "ParameterNames" ) and result.has_key( "Records" ):
            if len( result["ParameterNames"] ) > 0:
              if len( result["Records"] ) > 0:
                callback = []
                jobs = result["Records"]
                head = result["ParameterNames"]
                headLength = len( head )
                for i in jobs:
                  tmp = {}
                  for j in range( 0, headLength ):
                    tmp[head[j]] = i[j]
                  callback.append( tmp )
                total = result["TotalRecords"]
                if result.has_key( "Extras" ):
                  extra = result["Extras"]
                  callback = {"success":"true", "result":callback, "total":total, "extra":extra}
                else:
                  callback = {"success":"true", "result":callback, "total":total}
                timestamp = Time.dateTime().strftime( "%Y-%m-%d %H:%M [UTC]" )
                callback["date"] = timestamp
              else:
                callback = {"success":"false", "result":"", "error":"There are no data to display"}
            else:
              callback = {"success":"false", "result":"", "error":"ParameterNames field is missing"}
          else:
            callback = {"success":"false", "result":"", "error":"Data structure is corrupted"}
        else:
          callback = {"success":"false", "result":"", "error":"There were no data matching your selection"}
      else:
        callback = {"success":"false", "result":"", "error":"Data structure is corrupted"}
    else:
      callback = {"success":"false", "error":result["Message"]}
    gLogger.info( "\033[0;31mJOB SUBMIT REQUEST:\033[0m %s" % ( time() - pagestart ) )
    self.finish( callback )

