import json
import urllib
import re

from DIRAC import S_ERROR, S_OK, gConfig, gLogger
from DIRAC.ConfigurationSystem.Client.Helpers.Operations import Operations
from DIRAC.ConfigurationSystem.Client import PathFinder
from DIRAC.Core.DISET.RPCClient import RPCClient
from WebAppDIRAC.Lib.WebHandler import WebHandler, asyncGen, WErr



softBaseURL = 'http://lhcbproject.web.cern.ch/lhcbproject/dist/'

def getSoftVersions( name, web_names = '', add = '' ):
  """ Return list of all downloadable versions
      of specified software package.
      web_name is default to name_name
      add specify verbatim version to add to the list
      Note: It was in ProductionLib
  """
  if web_names == '':
    web_names = name + '_' + name
# Download the page
  url = softBaseURL + name + '/'
  try:
    f = urllib.urlopen( url )
    page = f.read()
    f.close()
  except Exception, e:
    return S_ERROR( "Can't download %s: %s" % ( url, str( e ) ) )
  uver = {}
  for web_name in web_names.split( ',' ):
    # Find all references to package
    ver = re.compile( web_name + '_([^_]+)\\.tar\\.gz' ).findall( page )
    # Remove duplicates
    for x in ver:
      uver[x] = 1
  for v in add.split( ',' ):
    if( v ):
      uver[v] = 1
  ver = uver.keys()
# Sort the result
  ver.sort()
# Newer first
  ver.reverse()
  return S_OK( ver )

class LHCbStepManagerHandler( WebHandler ):
  
  AUTH_PROPS = "authenticated"

  def __getArgument( self, argName, argDefValue ):
    # ToDo: I think it should be somewhere in DIRAC
    if argName not in self.request.arguments:
      return argDefValue
    if len( self.request.arguments[argName] ) > 0:
      return self.request.arguments[argName][0]
    return argDefValue

  def __getJsonArgument( self, argName, argDefValue ):
    # ToDo: I think it should be somewhere in DIRAC
    if argName not in self.request.arguments:
      return argDefValue
    if len( self.request.arguments[argName] ) > 0 and self.request.arguments[argName][0] != '':
      return json.loads( self.request.arguments[argName][0] )
    return argDefValue

  def __getApplications( self ):
    applications = []
    csS = PathFinder.getServiceSection( 'ProductionManagement/ProductionRequest' )
    if csS:
      applications = gConfig.getValue( '%s/Applications' % csS, '' )
    if not applications:
      applications = ["Gauss", "Boole", "Brunel", "DaVinci", "Moore", "LHCb"]
    else:
      applications = applications.split( ', ' )
    return applications

  def web_getApplications( self ):
    applications = self.__getApplications()

    self.write( { "success": "true", "total": len( applications ), "result": [ { 'v' : x } for x in applications ] } )

  @asyncGen
  def web_getAppVersions( self ):
    app = self.__getArgument( 'app', '' )
    if not app:
      self.finish( {"success" : "true", "result" : [], "total" : 0} )
      return

    result = yield self.threadTask( getSoftVersions, app )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "total" : 0,
                    "error" : result["Message"]} )
      return
    
    versions = result["Value"]
    self.finish( { "success": "true", "total": len( versions ), "result": [ { 'v' : x } for x in versions ] } )

  def web_getAppOptionsFormats( self ):
    app = self.__getArgument( 'app', '' )
    if not app:
      self.write( {"success" : "true", "result" : [], "total" : 0} )
      return

    formats = Operations().getValue( 'Productions/StepOptionsFormat/%s' % app )
    if formats is not None:
      formats = formats.split( ', ' )
    else:
      formats = []
    self.finish( { "success": "true", "total": len( formats ), "result": [ { 'v' : x } for x in formats ] } )


  def web_getSelectionData( self ):
    callback = {}

    callback['ApplicationName'] = [ [ x ] for x in self.__getApplications() ]
    callback['Visible'] = [ ["Yes"], ["No"], ]
    callback['Usable'] = [ ["Yes"], ["Not ready"], ["Obsolete"] ]

    self.write( callback )


  def __getFilter( self ):
    visibleMap = { 'Yes' : 'Y', 'No' : 'N' }
    filter = {}

    # Selector
    try:
                
      for selFieldName in ['ApplicationName', 'Visible', 'Usable',
                           'ProcessingPass', 'StartDate', 'StartDate', 'InputFileTypes', 'OutputFileTypes', 'Equal']:
        field = self.__getJsonArgument( selFieldName, [] )
        if len( field ) > 0:
          if selFieldName == 'Equal':
            filter[selFieldName] = str(field[-1]) 
          elif selFieldName == 'StartDate':
            filter[selFieldName] = str( field[0] )
          elif selFieldName == 'Visible':
            filter[selFieldName] = [ visibleMap.get( str( x ), 'Y' ) for x in field ]
          else:
            filter[selFieldName] = [ str( x ) for x in field ]
          # gLogger.info( "%s = %s" % ( selFieldName, str(field) ) )
    except Exception, e:
      gLogger.info( '__getFilter: Wrong selection: %s' % str( e ) )

    # Grid ordering and sorting
    sort = { 'Items' : 'StepId', 'Order' : 'Desc' }
    try:
      start = int( self.__getArgument( 'start', 0 ) )
      limit = int( self.__getArgument( 'limit', 0 ) )
      sortlist = self.__getJsonArgument( 'sort', [] )
      if len( sortlist ) > 0 :
        sort = { 'Items' : str( sortlist[-1]['property'] ),
                 'Order' : str( sortlist[-1]['direction'] )}
    except Exception, e:  # fallback to defaults instead of error
      start = 0
      limit = 25
    if limit > 0:
      filter['StartItem'] = start
      filter['MaxItem'] = start + limit
    filter['Sort'] = sort
    gLogger.info( str(filter) )
    return filter

  def __runtimeProjectsConvert( self, step ):
    projects = step.get( 'RuntimeProjects', [] )
    if projects:
      fieldNames = projects['ParameterNames']
      projects = [dict( zip( fieldNames, y ) ) for y in projects['Records']]
    step['RuntimeProjects'] = projects
    step['textRuntimeProjects'] = \
        ','.join( ["%s(%s)" % ( x['StepName'], x['StepId'] ) for x in projects] )

  def __nullConvert( self, oneDict ):
    for x in oneDict:
      if oneDict[x] is None:
        oneDict[x] = ""

  @asyncGen
  def web_getSteps( self ):
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )
    filter = self.__getFilter()
    
    result = yield self.threadTask( RPC.getAvailableSteps, filter )
    
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "total" : 0,
                    "error" : result["Message"]} )
      return
    
    result = result["Value"]

    if "TotalRecords" in result:
      if not ( result["TotalRecords"] > 0 ):
        raise WErr(500, "There were no data matching your selection")
    else:
      raise WErr(500, "There were no data matching your selection")
        

    fields = result['ParameterNames']
    steps = [dict( zip( fields, x ) ) for x in result['Records']]
    for step in steps:
      self.__runtimeProjectsConvert( step )

    self.finish( {"success" : "true", "result" : steps,
                  "total" : result["TotalRecords"], "date" : None} )

  @asyncGen
  def web_getStep( self ):
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )

    StepId = 0
    try:
      StepId = int( self.__getArgument( 'StepId', 0 ) )
    except Exception, e:
      pass  # ignore errors

    # Get Step Body
    result = yield self.threadTask( RPC.getAvailableSteps, { 'StepId' : StepId } )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
      return
    result = result["Value"]
    fields = result['ParameterNames']
    steps = [dict( zip( fields, x ) ) for x in result['Records']]
    if len( steps ) != 1:
      self.finish( {"success" : "false", "result" : [], "error" : "Requested Step is not found"} )
      return
    step = steps[0]

    # Get Input Files for the step
    result = RPC.getStepInputFiles( StepId )
    if not result['OK']:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
      return
    result = result["Value"]
    fields = result['ParameterNames']
    ift = [dict( zip( fields, x ) ) for x in result['Records']]

    # Get Output Files for the step
    result = RPC.getStepOutputFiles( StepId )
    if not result['OK']:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
      return
    result = result["Value"]
    fields = result['ParameterNames']
    oft = [dict( zip( fields, x ) ) for x in result['Records']]

    # Put everything together
    step['InputFileTypes'] = ift
    step['OutputFileTypes'] = oft
    try:
      step['textInputFileTypes'] = \
        ','.join( ["%s(%s)" % ( x['FileType'], x['Visible'] ) for x in ift] )
      step['textOutputFileTypes'] = \
        ','.join( ["%s(%s)" % ( x['FileType'], x['Visible'] ) for x in oft] )
    except Exception, e:
      self.finish( {"success" : "false", "result" : [], "error" : "Can not convert File Types: %s" % str( e )} )
    self.__runtimeProjectsConvert( step )
    self.__nullConvert( step )

    self.finish( {"success" : "true", "result" : step} )

  @asyncGen
  def web_getRuntimeProjects( self ):
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )
    result = yield self.threadTask( RPC.getAvailableSteps, {} )
    result = RPC.getAvailableSteps( {} )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
      return

    fields = result['Value']['ParameterNames']
    rows = [dict( zip( fields, x ) ) for x in result['Value']['Records']]
    rows = [{'id':r['StepId'], 'text': '%s(%s)' % ( r['StepName'], r['StepId'] ) } for r in rows if r['Usable'] == 'Yes' ]
    rows.sort( key = lambda x:x['id'], reverse = True )
    self.finish( { "success": "true", "result": rows, 'total': len( rows ) } )

  @asyncGen
  def web_getBKTags( self ):
    tag = self.__getArgument( 'tag', '' )
    if not tag:
      self.finish( {"success" : "true", "result" : [], "total" : 0} )
      return
    tags = tag.split( ':' )

    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )
    result = yield self.threadTask( RPC.getAvailableTags )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "total" : 0,
                    "error" : result["Message"]} )
      return

    rows = [{'v':x[1]} for x in result['Value']['Records'] if x[0] in tags]
    rows.sort( key = lambda x: x['v'] )
    rows.append( {'v':'ONLINE'} )
    rows.append( {'v':'fromPreviousStep'} )
    self.finish( { "success": "true", "total": len( rows ), "result": rows } )

  @asyncGen
  def web_getFileTypes( self ):
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )
    result = yield self.threadTask( RPC.getAvailableFileTypes )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "total" : 0,
                    "error" : result["Message"]} )
      return
    rows = [dict( zip( [ 'Name', 'Description'], x ) ) for x in result['Value']]
    rows.sort( key = lambda x: x['Name'] )
    self.finish( { "success": "true", "total": len( rows ), "result": rows } )

  @asyncGen
  def web_addFileType( self ):
    name = self.__getArgument( 'Name', '' )
    description = self.__getArgument( 'Description', '' )
    if not name or not description:
      self.finish( {"success" : "false", "result" : [],
                    "error" : "File type specification is incomplete"} )
      return
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )
    result = yield self.threadTask( RPC.insertFileTypes, str( name ), str( description ), 'ROOT' )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
    else:
      self.finish( {"success" : "true", "result" : [] } )

  __stepOrdinaryFields = [ 'StepId', 'StepName', 'ApplicationName', 'ApplicationVersion', 'SystemConfig', 'mcTCK',
                           'OptionFiles', 'OptionsFormat', 'DDDB', 'CONDDB', 'DQTag', 'ExtraPackages',
                           'Visible', 'Usable', 'ProcessingPass', 'isMulticore' ]

  def __decodeFileTypes( self, s ):
    r = []
    try:
      ftl = json.loads( s )
      for x in ftl:
        d = {}
        for y in x:
          d[str( y )] = str( x[y] )
        r.append( d )
    except Exception, e:
      gLogger.error( "Cound not covert 's': %s" % str( e ) )
    return r


  @asyncGen
  def web_saveStep( self ):
    params = {}
    for name in self.request.arguments:
      value = self.request.arguments[name][0]
      if name in self.__stepOrdinaryFields:
        if isinstance( value, unicode ):
          value = str( value )
        params[name] = value
      elif name in [ 'InputFileTypes', 'OutputFileTypes' ]:
        params[name] = self.__decodeFileTypes( value )
      elif name in [ 'RuntimeProjectStepId' ]:
        if value:
          params['RuntimeProjects'] = [ {'StepId':int( value )} ]
    if 'RuntimeProjects' not in params:
      params['RuntimeProjects'] = []

    if 'StepId' not in params:
      params['StepId'] = '0'

    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )    
    if params['StepId'] is '0':
      for name in [ 'StepId', 'InputFileTypes', 'OutputFileTypes', 'RuntimeProjects' ]:
        if name in params and not params[name]:
          del params[name]
      params = { 'Step': params }
      for name in [ 'InputFileTypes', 'OutputFileTypes' ]:
        if name in params['Step']:
          params[name] = params['Step'][name]
          del params['Step'][name]
      result = yield self.threadTask( RPC.insertStep, params )
    else:
      result = yield self.threadTask( RPC.updateStep, params )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
    else:
      self.finish( {"success" : "true", "result" : [] } )

  @asyncGen
  def web_deleteStep( self ):
    StepId = 0
    try:
      StepId = int( self.__getArgument( 'StepId', 0 ) )
    except Exception, e:
      pass
    if not StepId:
      self.finish( {"success" : "false", "result" : [], "error" : "StepId is not correctly specified"} )
      return
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager', timeout = 600 )
    result = yield self.threadTask( RPC.deleteStep, StepId )
    if not result["OK"]:
      self.finish( {"success" : "false", "result" : [], "error" : result["Message"]} )
    else:
      self.finish( {"success" : "true", "result" : [] } )
