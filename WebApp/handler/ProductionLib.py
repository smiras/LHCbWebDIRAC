import urllib
import re
import json

from DIRAC import S_ERROR,S_OK

class PrTpl(object):
  """ Production Template engine
  AZ: I know it is not the best :)
  It undestand: {{<sep><ParName>[#<Label>[#<DefaultValue>]]}}
  Where:
    <sep> - not alpha character to be inserted
            before parameter value in case it
            is not empty
    <ParName> - parameter name (must begin
            with alpha character) to substitute
    <Label> - if '#' is found after '{{', everything after
            it till '#' or '}}' is the Label for the parameter
            (default to ParName) In case you use one parameter
            several times, you can specify the label only once.
    <DefaultValue> - the value in the disalog by default
            (empty string if not specified)
  """
  __par_re  = "\{\{(\W)?(.+?)(|(#.*?))\}\}"
  __par_i_re = "#([^\#]*)#?(.*)"
  __par_sub = "\{\{(\W|)?%s(|(#.*?))\}\}"
  def __init__(self,tpl_xml):
    self.tpl = tpl_xml
    self.pdict = {}
    self.ddict = {}
    for x in re.findall(self.__par_re,self.tpl):
      rest = re.findall(self.__par_i_re,x[3])
      if rest:
        rest = rest[0]
      else:
        rest = ('','')
      if not x[1] in self.ddict or rest[1]:
        self.ddict[x[1]] = rest[1]
      if x[1] in self.pdict and not rest[0]:
        continue
      label = rest[0]
      if not label:
        label = x[1]
      self.pdict[x[1]] = label

  def getParams(self):
    """ Return the dictionary with parameters (value is label) """
    return self.pdict

  def getDefaults(self):
    """ Return the dictionary with parameters defaults """
    return self.ddict

  def apply(self,pdict):
    """ Return template with substituted values from pdict """
    result = self.tpl
    for p in self.pdict:
      value = str(pdict.get(p,''))
      if value:
        value = "\\g<1>" + value
      result = re.sub(self.__par_sub % p, value, result)
    return result

softBaseURL = 'http://lhcbproject.web.cern.ch/lhcbproject/dist/'

def getSoftVersions(name, web_names='',add=''):
  """ Return list of all downloadable versions
      of specified software package.
      web_name is default to name_name
      add specify verbatim version to add to the list
  """
  if web_names == '':
    web_names = name + '_' + name
# Download the page
  url = softBaseURL+name+'/'
  try:
    f = urllib.urlopen(url)
    page = f.read()
    f.close()
  except Exception, e:
    return S_ERROR("Can't download %s: %s"%(url,str(e)))
  uver = {}
  for web_name in web_names.split(','):
    # Find all references to package
    ver = re.compile(web_name+'_([^_]+)\\.tar\\.gz').findall(page)
    # Remove duplicates
    for x in ver:
      uver[x] = 1
  for v in add.split(','):
    if(v):
      uver[v] = 1
  ver = uver.keys()
# Sort the result
  ver.sort()
# Newer first
  ver.reverse()
  return S_OK(ver)

def SelectAndSort(request,rows,default_sort):
  """ Can be used to return extJS adopted list of rows
  """
  total = len(rows)
  if total == 0:
    return { 'OK':True, 'result':[], 'total': 0 }
  try:    
    start = int(request.arguments.get('start', 0)[-1])
  
    if start != 0:
      start = int(json.loads(request.arguments.get('start')[-1]))
    
    limit = int(request.arguments.get('limit', total)[-1])
    if limit != total:
      limit = int(json.loads(request.arguments.get('limit', total)[-1]))
    
    sortList  = request.arguments.get('sort', default_sort)[-1]
    dir   = 'ASC'
    if sortList != default_sort:
      sortList  = json.loads(request.arguments.get('sort', default_sort)[-1])
      dir = sortList[0]['direction']
      sort = sortList[0]['property']
      
    if not sort in rows[0]:
      raise Exception('Sort field '+sort+' is not found')
  except Exception, e:
    return S_ERROR('Badly formatted list request: '+str(e))
  
  rows.sort(key=lambda x: x[sort], reverse=(dir!='ASC'))
  return { 'OK':True, 'result':rows[start:start+limit], 'total': total }

