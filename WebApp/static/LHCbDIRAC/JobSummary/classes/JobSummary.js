/**
 * It is used to open the LHCb specific PilotMonitor application
 */
Ext.define('LHCbDIRAC.JobSummary.classes.JobSummary', {
      extend : 'DIRAC.JobSummary.classes.JobSummary',
      initComponent : function() {

        var me = this;
        me.callParent();
        me.launcher.title = "Job Summary";
        me.applicationsToOpen = {
          'JobMonitor' : 'LHCbDIRAC.LHCbJobMonitor.classes.LHCbJobMonitor'
        }
      }
    });