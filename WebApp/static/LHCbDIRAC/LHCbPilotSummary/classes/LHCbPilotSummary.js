/**
 * It is used to open the LHCb specific PilotMonitor application
 */
Ext.define('LHCbDIRAC.LHCbPilotSummary.classes.LHCbPilotSummary', {
      extend : 'DIRAC.PilotSummary.classes.PilotSummary',
      initComponent : function() {

        var me = this;
        me.callParent();
        me.launcher.title = "LHCb Pilot Summary";
        me.applicationsToOpen = {
          'PilotMonitor' : 'LHCbDIRAC.LHCbPilotMonitor.classes.LHCbPilotMonitor'
        }

      }
    });