Ext.define('LHCbDIRAC.ProductionRequestManager.classes.TemplateDetail', {
      extend : 'Ext.panel.Panel',
      tplMarkup : ['<b>Name:</b> {display_name}<br/>', '<b>Short description:</b> {Description}<br/>', '<b>Author:</b> {Author}<br/>', '<b>Last modified:</b> {PublishingTime}<br/>', '<b>Description:</b> {LongDescription}<br/>'],

      initComponent : function() {
        var me = this;
        me.tpl = new Ext.Template(me.tplMarkup);
        me.data = {};
        me.callParent(arguments);
      },

      updateDetail : function(data) {
        var me = this;
        me.data = data;
        me.data.display_name = data.WFName.replace(/_wizard\.py/, '');
        me.tpl.overwrite(me.body, me.data);
      }
    });